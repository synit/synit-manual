# Shell-scripting libraries

The `syndicate-sh` package includes `/usr/lib/syndicate/syndicate.sh`, an implementation of the
[Syndicate Protocol](../protocol.md) for Bash. Scripts may take advantage of the library to
interact with peers via system dataspaces, either as [supervised
services](../operation/builtin/daemon.md) or as external programs making use of the [gatekeeper
service](../operation/builtin/gatekeeper.md).

Examples of both kinds of script are included in the [`syndicate-sh` git
repository](https://git.syndicate-lang.org/syndicate-lang/syndicate-sh) (see the
[examples](https://git.syndicate-lang.org/syndicate-lang/syndicate-sh/src/branch/main/examples)
directory).
