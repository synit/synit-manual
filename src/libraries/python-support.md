# Python support libraries

The `py3-preserves` and `py3-syndicate` packages include the Python implementations of
Preserves ([`preserves` on PyPI](https://pypi.org/project/preserves/);
[git](https://gitlab.com/preserves/preserves/tree/main/implementations/python/)) and the
[Syndicated Actor Model](../glossary.md#syndicated-actor-model) and [Syndicate
Protocol](../protocol.md) ([`syndicate-py` on PyPI](https://pypi.org/project/syndicate-py/);
[git](https://git.syndicate-lang.org/syndicate-lang/syndicate-py)), respectively.

When installed, the libraries are available in the standard location for system-wide Python
packages.
