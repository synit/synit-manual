#![allow(unused_parens)]
#![allow(unused_imports)]

use std::convert::TryFrom;
use preserves_schema::support as _support;
use _support::Deserialize;
use _support::Parse;
use _support::Unparse;
use _support::preserves;
use preserves::value::Domain;
use preserves::value::NestedValue;

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Present {
    pub username: std::string::String
}

impl preserves::value::Domain for Present {}

impl<_Value: preserves::value::NestedValue> _support::Deserialize<_Value> for Present {
    fn deserialize<'de, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        let mut _tmp0 = _support::B::Type::default();
        _tmp0.shift(Some(_support::B::Item::RecordLabel));
        r.boundary(&_tmp0)?;
        match r.next_token(true)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "Present" => {}
                _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.Present"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.Present"))?,
        }
        let _tmp1 = ();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp2 = r.next_str()?.into_owned();
        r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
        Ok(Present {username: _tmp2})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Present {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_0_PRESENT { return Err(_support::ParseError::conformance_error("simpleChatProtocol.Present")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("simpleChatProtocol.Present")); }
        let _tmp2 = (&_tmp0.fields()[0]).value().to_string()?;
        Ok(Present {username: _tmp2.clone()})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Present {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let Present {username: _tmp0} = self;
        {
            let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_0_PRESENT).clone()]);
            _tmp1.fields_vec_mut().push(preserves::value::Value::from(_tmp0).wrap());
            _tmp1.finish().wrap()
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Says {
    pub who: std::string::String,
    pub what: std::string::String
}

impl preserves::value::Domain for Says {}

impl<_Value: preserves::value::NestedValue> _support::Deserialize<_Value> for Says {
    fn deserialize<'de, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        let mut _tmp0 = _support::B::Type::default();
        _tmp0.shift(Some(_support::B::Item::RecordLabel));
        r.boundary(&_tmp0)?;
        match r.next_token(true)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "Says" => {}
                _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.Says"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.Says"))?,
        }
        let _tmp1 = ();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp2 = r.next_str()?.into_owned();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp3 = r.next_str()?.into_owned();
        r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
        Ok(Says {who: _tmp2, what: _tmp3})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Says {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_1_SAYS { return Err(_support::ParseError::conformance_error("simpleChatProtocol.Says")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("simpleChatProtocol.Says")); }
        let _tmp2 = (&_tmp0.fields()[0]).value().to_string()?;
        let _tmp3 = (&_tmp0.fields()[1]).value().to_string()?;
        Ok(Says {who: _tmp2.clone(), what: _tmp3.clone()})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Says {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let Says {who: _tmp0, what: _tmp1} = self;
        {
            let mut _tmp2 = preserves::value::Record(vec![(&<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_1_SAYS).clone()]);
            _tmp2.fields_vec_mut().push(preserves::value::Value::from(_tmp0).wrap());
            _tmp2.fields_vec_mut().push(preserves::value::Value::from(_tmp1).wrap());
            _tmp2.finish().wrap()
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum Status {
    Here,
    Away {
        since: std::boxed::Box<TimeStamp>
    }
}

impl preserves::value::Domain for Status {}

fn read_status_here<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Status, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "here" => {}
            _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.Status::here"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.Status::here"))?,
    }
    let _tmp0 = ();
    Ok(Status::Here)
}

fn read_status_away<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Status, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "away" => {}
            _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.Status::away"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.Status::away"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp2 = TimeStamp::deserialize(r)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(Status::Away {since: std::boxed::Box::new(_tmp2)})
}

impl<_Value: preserves::value::NestedValue> _support::Deserialize<_Value> for Status {
    fn deserialize<'de, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_status_here(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_status_away(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("simpleChatProtocol.Status"))
    }
}

fn parse_status_here<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<Status, _support::ParseError> {
    if value != &<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_2_HERE { return Err(_support::ParseError::conformance_error("simpleChatProtocol.Status::here")); }
    let _tmp0 = ();
    Ok(Status::Here)
}

fn parse_status_away<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<Status, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_3_AWAY { return Err(_support::ParseError::conformance_error("simpleChatProtocol.Status::away")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("simpleChatProtocol.Status::away")); }
    let _tmp2 = TimeStamp::parse(_ctxt, (&_tmp0.fields()[0]))?;
    Ok(Status::Away {since: std::boxed::Box::new(_tmp2)})
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Status {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = parse_status_here(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_status_away(_ctxt, value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("simpleChatProtocol.Status"))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Status {
    fn unparse(&self, _ctxt: _L) -> _Value {
        match self {
            Status::Here => (&<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_2_HERE).clone(),
            Status::Away {since: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_3_AWAY).clone()]);
                _tmp1.fields_vec_mut().push(_tmp0.as_ref().unparse(_ctxt));
                _tmp1.finish().wrap()
            },
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct TimeStamp(pub std::string::String);

impl preserves::value::Domain for TimeStamp {}

impl<_Value: preserves::value::NestedValue> _support::Deserialize<_Value> for TimeStamp {
    fn deserialize<'de, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = r.next_str()?.into_owned();
        Ok(TimeStamp(_tmp0))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for TimeStamp {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_string()?;
        Ok(TimeStamp(_tmp0.clone()))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for TimeStamp {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let TimeStamp(_tmp0) = self;
        preserves::value::Value::from(_tmp0).wrap()
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct UserStatus {
    pub username: std::string::String,
    pub status: Status
}

impl preserves::value::Domain for UserStatus {}

impl<_Value: preserves::value::NestedValue> _support::Deserialize<_Value> for UserStatus {
    fn deserialize<'de, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        let mut _tmp0 = _support::B::Type::default();
        _tmp0.shift(Some(_support::B::Item::RecordLabel));
        r.boundary(&_tmp0)?;
        match r.next_token(true)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "Status" => {}
                _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.UserStatus"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("simpleChatProtocol.UserStatus"))?,
        }
        let _tmp1 = ();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp2 = r.next_str()?.into_owned();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp3 = Status::deserialize(r)?;
        r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
        Ok(UserStatus {username: _tmp2, status: _tmp3})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for UserStatus {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_4_STATUS { return Err(_support::ParseError::conformance_error("simpleChatProtocol.UserStatus")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("simpleChatProtocol.UserStatus")); }
        let _tmp2 = (&_tmp0.fields()[0]).value().to_string()?;
        let _tmp3 = Status::parse(_ctxt, (&_tmp0.fields()[1]))?;
        Ok(UserStatus {username: _tmp2.clone(), status: _tmp3})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a chat::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for UserStatus {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let UserStatus {username: _tmp0, status: _tmp1} = self;
        {
            let mut _tmp2 = preserves::value::Record(vec![(&<_L as Into<&'a chat::Language<_Value>>>::into(_ctxt).LIT_4_STATUS).clone()]);
            _tmp2.fields_vec_mut().push(preserves::value::Value::from(_tmp0).wrap());
            _tmp2.fields_vec_mut().push(_tmp1.unparse(_ctxt));
            _tmp2.finish().wrap()
        }
    }
}
