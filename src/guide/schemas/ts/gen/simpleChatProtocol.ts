import * as _ from "@preserves/core";

export const $Present = _.Symbol.for("Present");
export const $Says = _.Symbol.for("Says");
export const $Status = _.Symbol.for("Status");
export const $away = _.Symbol.for("away");
export const $here = _.Symbol.for("here");

let __schema: _.Value | null = null;

export function _schema() {
    if (__schema === null) {
        __schema = _.decode<_.GenericEmbedded>(_.Bytes.fromHex("b4b306736368656d61b7b30776657273696f6eb00101b30b646566696e6974696f6e73b7b30453617973b4b303726563b4b3036c6974b3045361797384b4b3057475706c65b5b4b3056e616d6564b30377686fb4b30461746f6db306537472696e678484b4b3056e616d6564b30477686174b4b30461746f6db306537472696e678484848484b306537461747573b4b3026f72b5b5b10468657265b4b3036c6974b304686572658484b5b10461776179b4b303726563b4b3036c6974b3046177617984b4b3057475706c65b5b4b3056e616d6564b30573696e6365b4b303726566b584b30954696d655374616d708484848484848484b30750726573656e74b4b303726563b4b3036c6974b30750726573656e7484b4b3057475706c65b5b4b3056e616d6564b308757365726e616d65b4b30461746f6db306537472696e678484848484b30954696d655374616d70b4b30461746f6db306537472696e6784b30a55736572537461747573b4b303726563b4b3036c6974b30653746174757384b4b3057475706c65b5b4b3056e616d6564b308757365726e616d65b4b30461746f6db306537472696e678484b4b3056e616d6564b306737461747573b4b303726566b584b306537461747573848484848484b30c656d62656464656454797065808484"));
    };
    return __schema;
}

export const _imports = {}


export type Present = {"username": string};

export type Says = {"who": string, "what": string};

export type UserStatus = {"username": string, "status": Status};

export type Status = ({"_variant": "here"} | {"_variant": "away", "since": TimeStamp});

export type TimeStamp = string;


export function Present(username: string): (
    Present &
    {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
) {return {"username": username, __as_preserve__() {return fromPresent(this);}};}

Present.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Present")};
}

export function Says({who, what}: {who: string, what: string}): (Says & {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}) {return {"who": who, "what": what, __as_preserve__() {return fromSays(this);}};}

Says.schema = function () {
    return {schema: _schema(), imports: _imports, definitionName: _.Symbol.for("Says")};
}

export function UserStatus({username, status}: {username: string, status: Status}): (
    UserStatus &
    {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
) {
    return {
        "username": username,
        "status": status,
        __as_preserve__() {return fromUserStatus(this);}
    };
}

UserStatus.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("UserStatus")
    };
}

export namespace Status {
    export function here(): (
        Status &
        {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
    ) {return {"_variant": "here", __as_preserve__() {return fromStatus(this);}};};
    here.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Status"),
            variant: _.Symbol.for("here")
        };
    };
    export function away(since: TimeStamp): (
        Status &
        {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
    ) {
        return {
            "_variant": "away",
            "since": since,
            __as_preserve__() {return fromStatus(this);}
        };
    };
    away.schema = function () {
        return {
            schema: _schema(),
            imports: _imports,
            definitionName: _.Symbol.for("Status"),
            variant: _.Symbol.for("away")
        };
    };
}

export function TimeStamp(value: string): TimeStamp {return value;}

TimeStamp.schema = function () {
    return {
        schema: _schema(),
        imports: _imports,
        definitionName: _.Symbol.for("TimeStamp")
    };
}

export function asPresent<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): (
    Present &
    {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
) {
    let result = toPresent(v);
    if (result === void 0) throw new TypeError(`Invalid Present: ${_.stringify(v)}`);
    return result;
}

export function toPresent<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | (
    Present &
    {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
) {
    let result: undefined | (
        Present &
        {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
    );
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $Present) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"username": _tmp1, __as_preserve__() {return fromPresent(this);}};};
        };
    };
    return result;
}

Present.__from_preserve__ = toPresent;

export function fromPresent<_embedded = _.GenericEmbedded>(_v: Present): _.Value<_embedded> {return _.Record($Present, [_v["username"]]);}

export function asSays<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): (Says & {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}) {
    let result = toSays(v);
    if (result === void 0) throw new TypeError(`Invalid Says: ${_.stringify(v)}`);
    return result;
}

export function toSays<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | (Says & {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}) {
    let result: undefined | (Says & {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>});
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $Says) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (string) | undefined;
                _tmp2 = typeof v[1] === 'string' ? v[1] : void 0;
                if (_tmp2 !== void 0) {
                    result = {"who": _tmp1, "what": _tmp2, __as_preserve__() {return fromSays(this);}};
                };
            };
        };
    };
    return result;
}

Says.__from_preserve__ = toSays;

export function fromSays<_embedded = _.GenericEmbedded>(_v: Says): _.Value<_embedded> {return _.Record($Says, [_v["who"], _v["what"]]);}

export function asUserStatus<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): (
    UserStatus &
    {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
) {
    let result = toUserStatus(v);
    if (result === void 0) throw new TypeError(`Invalid UserStatus: ${_.stringify(v)}`);
    return result;
}

export function toUserStatus<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | (
    UserStatus &
    {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
) {
    let result: undefined | (
        UserStatus &
        {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
    );
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: ({}) | undefined;
        _tmp0 = _.is(v.label, $Status) ? {} : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (Status) | undefined;
                _tmp2 = toStatus(v[1]);
                if (_tmp2 !== void 0) {
                    result = {
                        "username": _tmp1,
                        "status": _tmp2,
                        __as_preserve__() {return fromUserStatus(this);}
                    };
                };
            };
        };
    };
    return result;
}

UserStatus.__from_preserve__ = toUserStatus;

export function fromUserStatus<_embedded = _.GenericEmbedded>(_v: UserStatus): _.Value<_embedded> {
    return _.Record($Status, [_v["username"], fromStatus<_embedded>(_v["status"])]);
}

export function asStatus<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): (
    Status &
    {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
) {
    let result = toStatus(v);
    if (result === void 0) throw new TypeError(`Invalid Status: ${_.stringify(v)}`);
    return result;
}

export function toStatus<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | (
    Status &
    {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
) {
    let _tmp0: ({}) | undefined;
    let result: undefined | (
        Status &
        {__as_preserve__<_embedded = _.GenericEmbedded>(): _.Value<_embedded>}
    );
    _tmp0 = _.is(v, $here) ? {} : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "here", __as_preserve__() {return fromStatus(this);}};};
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp1: ({}) | undefined;
            _tmp1 = _.is(v.label, $away) ? {} : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (TimeStamp) | undefined;
                _tmp2 = toTimeStamp(v[0]);
                if (_tmp2 !== void 0) {
                    result = {
                        "_variant": "away",
                        "since": _tmp2,
                        __as_preserve__() {return fromStatus(this);}
                    };
                };
            };
        };
    };
    return result;
}

export namespace Status {export const __from_preserve__ = toStatus;}

export function fromStatus<_embedded = _.GenericEmbedded>(_v: Status): _.Value<_embedded> {
    switch (_v._variant) {
        case "here": {return $here;};
        case "away": {return _.Record($away, [fromTimeStamp<_embedded>(_v["since"])]);};
    };
}

export function asTimeStamp<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): TimeStamp {
    let result = toTimeStamp(v);
    if (result === void 0) throw new TypeError(`Invalid TimeStamp: ${_.stringify(v)}`);
    return result;
}

export function toTimeStamp<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | TimeStamp {
    let _tmp0: (string) | undefined;
    let result: undefined | TimeStamp;
    _tmp0 = typeof v === 'string' ? v : void 0;
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

TimeStamp.__from_preserve__ = toTimeStamp;

export function fromTimeStamp<_embedded = _.GenericEmbedded>(_v: TimeStamp): _.Value<_embedded> {return _v;}

