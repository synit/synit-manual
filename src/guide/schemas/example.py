from preserves import stringify, schema, parse, Symbol
S = schema.load_schema_file('./simpleChatProtocol.prb')
P = S.simpleChatProtocol

def hook_for_doctests():
    '''
    >>> P.Present('me')
    Present {'username': 'me'}

    >>> stringify(P.Present('me'))
    '<Present "me">'

    >>> P.Present.decode(parse('<Present "me">'))
    Present {'username': 'me'}

    >>> P.Present.try_decode(parse('<Present "me">'))
    Present {'username': 'me'}

    >>> P.Present.try_decode(parse('<NotPresent "me">')) is None
    True

    >>> stringify(P.UserStatus('me', P.Status.here()))
    '<Status "me" here>'

    >>> stringify(P.UserStatus('me', P.Status.away('2022-03-08')))
    '<Status "me" <away "2022-03-08">>'

    >>> x = P.UserStatus.decode(parse('<Status "me" <away "2022-03-08">>'))
    >>> x.status.VARIANT
    #away
    >>> x.status.VARIANT == Symbol('away')
    True
    '''
    pass

if __name__ == '__main__':
    import doctest
    doctest.testmod()
