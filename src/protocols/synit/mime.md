# MIME type support

 - [`[synit]/protocols/schemas/mime.prs`](https://git.syndicate-lang.org/synit/synit/src/branch/main/protocols/schemas/mime.prs)

Various protocols (e.g. the [sound effects](./soundEffects.md) protocols) rely on the
definition of a [MIME](https://en.wikipedia.org/wiki/MIME) `Value`, compatible with the
[suggested Preserves
convention](https://preserves.dev/conventions.html#mime-type-tagged-binary-data) for MIME
values.

The `type` is a symbol describing the MIME type, for example `application/octet-stream` or
`text/plain`. The `data` is the associated binary data, expected to be in the correct format
for the given `type`.

```
Value = <mime @type symbol @data bytes> .
```
