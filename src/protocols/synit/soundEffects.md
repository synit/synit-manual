# Sound effects

 - [`[synit]/protocols/schemas/soundEffects.prs`](https://git.syndicate-lang.org/synit/synit/src/branch/main/protocols/schemas/soundEffects.prs)

From time to time, it makes sense for the device to use sound to communicate with the user.

**Implementation.** As of October 2022, the implementation resides within classes
`RingToneDaemon` and `SoundEffectsDaemon` in the SqueakPhone Smalltalk image.

## Continuous sounds

Continuous sounds such as a user's ringtone may be produced by asserting `ContinuousSound` with
an appropriately-type-tagged sound file (such as an mp3 file) as `data`. In response, the sound
effects daemon play the file in a loop and will assert `ContinuousSoundPlaying`.

```
ContinuousSound = <continuous-sound @data mime.Value> .
ContinuousSoundPlaying = <continuous-sound-playing> .
```

## <span id="AlertSoundPlaying"></span>Alert sounds

One-off sound effects such as the notification of a received SMS may be produced by sending an
`AlertSound` message with an appropriately-type-tagged sound file (such as an mp3 file) as
`data`. In response, the sound effects daemon will play the file once and will assert
`AlertSoundPlaying` until the playback is complete.

```
AlertSound = <alert-sound @data mime.Value> .
AlertSoundPlaying = <alert-sound-playing> .
```
