# User settings

 - [`[synit]/protocols/schemas/userSettings.prs`](https://git.syndicate-lang.org/synit/synit/src/branch/main/protocols/schemas/userSettings.prs)

**Implementation.** The
[`user-settings-daemon`](https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-config/files/usr/lib/synit/python/synit/daemon/user_settings_daemon.py)
program reacts to `CommandRPC` and `CommandEvent` requests by updating files in its configured
`settingsDir`. See [here](../../operation/synit-config.md#user-settings) for more information
on the `user-settings-daemon`.

## Updating user settings

Assert `CommandRPC` containing an `action` for the settings daemon to perform and a `reply`
entity-reference for it to tell you when it is done. After it completes the `action`, it will
assert a `CommandReply` using the `reply` capability. Alternatively, send a `CommandEvent`
message containing an `action` if you do not require notification of completion.

```
CommandRPC = <user-settings-command @action Action @reply #:CommandReply> .
CommandEvent = <user-settings-command @action Action> .
CommandReply = =done .
Action = <assert @item any> / <retract @item any> .
```

See also [this how-to](../../operation/howto/manage-user-settings.md).

## Reading user settings

Configured user settings are published to the main `$config` dataspace contained within a
`Value` record to reliably distinguish them from system assertions.

```
Value = <user-setting @item any> .
```
