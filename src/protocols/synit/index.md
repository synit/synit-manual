# Synit schemas

 - Schema definitions: [`[synit]/protocols/schemas/`](https://git.syndicate-lang.org/synit/synit/src/branch/main/protocols/schemas/)

The following pages document schemas associated with the [Synit system
layer](../../syndicated-actor-model.md) and contained in the [synit Git
repository](https://git.syndicate-lang.org/synit/synit/src/branch/main/protocols/schemas/).
