# Time stamps

 - [`[synit]/protocols/schemas/time.prs`](https://git.syndicate-lang.org/synit/synit/src/branch/main/protocols/schemas/time.prs)

Various protocols rely on the definition of a time stamp type, `Stamp`, compatible with the
[suggested Preserves convention](https://preserves.dev/conventions.html#dates-and-times) for
dates and times.

```
Stamp = <rfc3339 @value string> .
```

From the Preserves convention document:

> Dates, times, moments, and timestamps can be represented with a Record with label `rfc3339`
> having a single field, a `String`, which MUST conform to one of the `full-date`,
> `partial-time`, `full-time`, or `date-time` productions of
> [section 5.6 of RFC 3339](https://datatracker.ietf.org/doc/html/rfc3339#section-5.6).
> (In `date-time`, “T” and
> “Z” MUST be upper-case and “T” MUST be used; a space separating the `full-date` and
> `full-time` MUST NOT be used.)
