# Modem support

 - [`[synit]/protocols/schemas/hayes.prs`](https://git.syndicate-lang.org/synit/synit/src/branch/main/protocols/schemas/hayes.prs)
 - [`[synit]/protocols/schemas/samsungGalaxyS7.prs`](https://git.syndicate-lang.org/synit/synit/src/branch/main/protocols/schemas/samsungGalaxyS7.prs)

Each particular class of modem has internal protocols needed for controlling it. So far, Synit
is able to interact with generic Hayes-style ("AT-command") modems, as seen in the PinePhone,
as well as with the modem in the Samsung Galaxy S7.

Each modem's internal protocol is spoken across the modem's distinguished dataspace, a
reference to which is held in the modem's [`ModemPresent`](./telephony.md#ModemPresent)
assertion.

## Hayes-style ("AT-command") Modems

As of October 2022, this protocol is implemented entirely in the SqueakPhone Smalltalk image,
in class `HayesModemActor` and friends.

### Presence

Hayes-style modems announce their presence with a subtype of the general `ModemPresent`
assertion schema.

```
ModemPresent = <modem =hayes @devicePath string @dataspace #:InternalProtocol> .
```

(TODO: specify the `InternalProtocol` properly)

```
InternalProtocol = any .
```

### Unsolicited Result Codes

An `UnsolicitedResultCode` message is sent when the modem sends us a "URC", an Unsolicited
Result Code.

```
UnsolicitedResultCode = <unsolicited @result Result> .
```

### Executing AT commands

Assert a `CommandRPC` record to request execution of an AT command string. The completion,
along with any responses, will be send to the `replyTo` entity reference. Alternatively, if no
completion notification or response is desired, send a `CommandEvent` message.

```
CommandRPC = <execute-command @commandText string @replyTo #:CommandResult> .
CommandEvent = <execute-command @commandText string> .
```

The result of a command execution is *asserted* as a `CommandResult` record to the `replyTo`
entity in the `CommandRPC`.

```
CommandResult = <command-result @commandText string @results [Result ...] @finalResult string> .
```

### Responses and Unsolicited Results

The `Result` type appears in both `UnsolicitedResultCode` and `CommandResult` records.

```
Result = <result @text string @tag MaybeString @fields MaybeStrings> .
MaybeString = @present string / @absent #f .
MaybeStrings = @present [string ...] / @absent #f .
```
**Examples.**
 - `<unsolicited <result "NO CARRIER" #f #f>>`
 - `<unsolicited <result "+CRING: VOICE" "CRING" ["VOICE"]>>`
 - `<unsolicited <result "+CLIP: \"+31655555555\",145,,,,0" "CLIP" ["+31655555555" "145" "" "" "" "0"]>>`
 - `<unsolicited <result "^DSCI: 2,1,4,0,+31655555555,145" "DSCI" ["2" "1" "4" "0" "+31655555555" "145"]>>`
 - `<unsolicited <result "+QIND: \"csq\",12,99" "QIND" ["csq" "12" "99"]>>`

## Samsung Galaxy S7

The Samsung Galaxy S7 was the first modem supported by Synit, but because of problems with the
relevant PostmarketOS kernel, support for it has languished a bit while development has
proceeded based around the PinePhone. As of October 2022, the Samsung modem support protocols
are implemented entirely in the SqueakPhone Smalltalk image, in class
`SamsungGalaxyS7ModemActor` and friends.

### Presence

The modem announces its presence with a subtype of the general `ModemPresent` assertion schema.

```
ModemPresent = <modem =samsung-galaxy-s7 @devicePath string @dataspace #:InternalProtocol> .
```

(TODO: specify the `InternalProtocol` properly)

```
InternalProtocol = any .
```

### Low-level packet I/O

```
ModemPacket = @in <from-modem @packet any> / @out <to-modem @packet any> .

# The bodies are instances of SamsungFmtMessage and SamsungRfsMessage, respectively.
FmtPacket = <fmt @body #:any> .
RfsPacket = <rfs @body #:any> .
```

### Executing commands

Analogous to AT command execution for Hayes-style modems.

```
# Assertion. Asks the modem to execute the given command.
CommandRPC = <execute-command @command FmtPacket @replyTo #:FmtPacket> .
# Message. Asks the modem to execute the given command, but not to send back the reply.
CommandEvent = <execute-command @command FmtPacket> .
```
