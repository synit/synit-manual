# Network core status and configuration

 - [`[synit]/protocols/schemas/network.prs`](https://git.syndicate-lang.org/synit/synit/src/branch/main/protocols/schemas/network.prs)

[interface monitor]: https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-config/files/usr/lib/synit/python/synit/daemon/interface_monitor.py
[wifi daemon]: https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-config/files/usr/lib/synit/python/synit/daemon/wifi_daemon.py
[network.pr]: https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-config/files/etc/syndicate/services/network.pr
[wifi.pr]: https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-config/files/etc/syndicate/services/wifi.pr
[modem.pr]: https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-config/files/etc/syndicate/services/modem.pr

The definitions in this protocol are used by the Synit daemons responsible for detecting and
responding to changes in the configuration of network devices.

**Implementation.** As of October 2022, this protocol is mostly implemented in
 - the [interface monitor][],
 - the [wifi daemon][], and
 - the scripting around [network state changes][network.pr].
 - the scripting around the [wifi subsystem][wifi.pr].
 - the scripting around [mobile data][modem.pr].

## Network interface presence and state

The [interface monitor][] daemon uses the Linux netlink protocol to monitor changes in
interface presence and state. In response to netlink messages, it maintains an `Interface`
assertion for each network interface in the system.

```
Interface = <interface
             @name string
             @index int
             @type InterfaceType
             @administrativeState AdministrativeState
             @operationalState OperationalState
             @carrier CarrierState
             @linkAddr string> .
```

The `InterfaceType` is synthesised by the interface monitor daemon based on somewhat ad-hoc
heuristics.

```
InterfaceType =
/ # `lo` and friends
  =loopback
/ # `eth0`, bridges, anything that isn't loopback and isn't wireless
  =normal
/ # 'wlan0' and friends
  =wireless
.
```

The `AdministrativeState` of an interface describes the state that the operator of the machine
wishes the interface to be in (according to the kernel).

```
AdministrativeState = =unknown / =down / =up .
```

The `OperationalState` and `CarrierState` taken together, by contrast, describe the *actual*
state of the interface.

```
OperationalState =
/ =unknown
/ =down
/ @lowerLayerDown =lower-layer-down
/ =testing
/ =dormant
/ =up
.

CarrierState =
/ @noCarrier =no-carrier
/ =carrier
.
```

## Route presence and state

The [interface monitor][] daemon also listens for netlink messages describing network route
presence and state changes, maintaining a `Route` assertion for each route present.

```
Route = <route
         @addressFamily AddressFamily
         @destination RouteDestination
         @priority int
         @typeOfService int
         @interfaceName RouteInterface
         @gateway Gateway> .

AddressFamily = =ipv4 / =ipv6 / @other int .
RouteDestination = =default / @prefix <prefix @net string @bits int> .
RouteInterface = @name string / @none #f .
Gateway = @addr string / @none #f .
```

## Default route presence

The [network.pr][] scripts respond to presence of `Route` records with a `default`
`destination` field by asserting a `DefaultRoute` record. Applications may use the presence or
absence of a `DefaultRoute` record to decide whether or not internet access is currently
present.

```
DefaultRoute = <default-route @addressFamily AddressFamily> .
```

## Wi-fi associations

The [wifi.pr][] script starts an instance of the [wifi daemon][] and an instance of
[`wpa_supplicant`](https://w1.fi/cgit/hostap/tree/README) for each `Interface` of type
`wireless`. The wifi daemon speaks with the `wpa_supplicant` instance to do network scanning as
well as managing network attachments. It publishes a `WifiAssociation` for each active
connection to a wifi access point.

```
WifiAssociation = <wifi-association @interfaceName string @ssid string @state WifiAssociationState> .
WifiAssociationState = @inProgress =in-progress / =ready .
```

## Hotspots

In future, the software will use `HotspotState` assertions to publish the presence and state of
wifi hotspots hosted by the device.

```
HotspotState = <hotspot-state @interfaceName string @ssid string @stationCount int> .
```

## AP scanning

In future, the software will assert `AvailableAccessPoint` with information it learns from
network scanning.

```
AvailableAccessPoint = <available-ap @interfaceName string @ssid string> .
```

## Network-related user settings

A number of user setting assertions control the network.

### Mobile Data

Asserting `MobileDataApn` and `MobileDataEnabled` causes the [modem.pr][] script to enable a
cellular data link.

```
MobileDataApn = <mobile-data-apn @apn string> .
MobileDataEnabled = <mobile-data-enabled> .
```

### Wifi network preferences

```
WifiAuthentication =
/ # No authentication necessary: open network
  =open
/ # Pre-shared key (WPA2-PSK etc)
  <psk @password string>
/ # Other, not-yet-implemented
  @other any
.
```

Each [wifi daemon][] responds to `SelectedWifiNetwork` assertions for its interface by trying
to connect to the named SSID using the provided credentials. The daemon asserts a
`WifiAssociation` in response.

```
SelectedWifiNetwork = <selected-wifi-network
                       @interfaceName string
                       @ssid string
                       @authentication WifiAuthentication> .
```

In future, user `Hotspot` assertions will cause setup of a wifi hotspot, hosted by the device.

```
Hotspot = <hotspot
           @interfaceName string
           @ssid string
           @authentication WifiAuthentication> .
```

In future, `SavedWifiNetwork` assertions will hold records of previously-configured wifi
connection details.

```
SavedWifiNetwork = <saved-wifi-network
                    @ssid string
                    @authentication WifiAuthentication
                    @priority double> .
```
