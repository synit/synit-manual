# Preserves Schema metaschema

The Preserves Schema metaschema defines the structure of the abstract syntax (AST) of schemas.
Every valid Preserves Schema document can be represented as an instance of the metaschema. (And
of course the metaschema is itself a schema, which is in turn an instance of the metaschema!)

⟶ See [Appendix A: "Metaschema" of the Preserves Schema
specification](https://preserves.dev/preserves-schema.html#appendix-metaschema).
