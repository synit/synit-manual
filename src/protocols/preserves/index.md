# Preserves schemas

The following pages document schemas associated with the [Preserves](../../guide/preserves.md)
data language and its tools.
