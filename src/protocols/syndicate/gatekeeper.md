# Gatekeeper and Sturdy-references

 - [`[syndicate-protocols]/schemas/gatekeeper.prs`](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/gatekeeper.prs)
 - [`[syndicate-protocols]/schemas/sturdy.prs`](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/sturdy.prs)

## Gatekeeper protocol

The Gatekeeper protocol allows a peer to "upgrade" a (relatively) long-lived certificate to a live reference to an
entity. For details of the use of `Resolve` and `Bind` assertions, see the [guide to the
built-in gatekeeper entity](../../operation/builtin/gatekeeper.md).

**Implementation.** The protocol is implemented within the
[`syndicate-server`](../../operation/system-bus.md) program.

```
Resolve = <resolve @step Step @observer #:Resolved> .
Bind = <bind @description Description @target #:any @observer BindObserver> .
BindObserver = @present #:Bound / @absent #f .

Resolved = <accepted @responderSession #:any> / Rejected .
Bound = <bound @pathStep PathStep> / Rejected .
Rejected = <rejected @detail any> .

Step = <<rec> @stepType symbol [@detail any]> .
Description = <<rec> @stepType symbol [@detail any]> .
```

## Sturdy-reference structures

A "sturdyref" is a long-lived certificate including a cryptographic signature that can be
upgraded by a gatekeeper entity to a live reference to the entity named in the sturdyref. For
an overview of `SturdyRef`s, see the [guide to the built-in gatekeeper
entity](../../operation/builtin/gatekeeper.md#sturdyrefs).

```
SturdyRef = <ref @parameters Parameters> .
Parameters = {
  oid: any,
  sig: bytes,
} & @caveats CaveatsField .

CaveatsField =
  / @present { caveats: [Caveat ...] }
  / @invalid { caveats: any }
  / @absent {} .

SturdyStepType = =ref .
SturdyStepDetail = Parameters .
SturdyDescriptionDetail = {
  oid: any,
  key: bytes,
} .
```

The `sig` in a `Parameters` is an iterated keyed-HMAC construction, starting from an HMAC of the
ref's *secret key* and its `oid`, following [macaroons][]. The specific function chosen is
[HMAC](https://www.rfc-editor.org/rfc/rfc2104) using
[BLAKE2s-256](https://www.rfc-editor.org/rfc/rfc7693), truncating the output to the first 16
bytes. Let

 - *f(k,d)* be `HMAC-BLAKE2s-256`*(k,d)*[0..16),
 - *e(v)* yield the canonical machine-oriented serialization of some preserves value *v*, and
 - *k* be the original secret key for the ref.

In a valid `SturdyRef`, then, the `sig` will be *f(...f(...f(f(k,e(*`oid`*)),...),e(*`Caveat`*)),...)*.

For detail of the interpretation of `Caveat`s, `Pattern`s, and `Template`s, see the [Syndicate
protocol specification](../../protocol.md#capabilities-on-the-wire).

```
Caveat = Rewrite / Alts / Reject / @unknown any .
Rewrite = <rewrite @pattern Pattern @template Template> .
Reject = <reject @pattern Pattern> .
Alts = <or @alternatives [Rewrite ...]>.

Lit = <lit @value any>.

Pattern = PDiscard / PAtom / PEmbedded / PBind / PAnd / PNot / Lit / PCompound .
PDiscard = <_>.
PAtom = =Boolean / =Double / =SignedInteger / =String / =ByteString / =Symbol .
PEmbedded = =Embedded .
PBind = <bind @pattern Pattern>.
PAnd = <and @patterns [Pattern ...]>.
PNot = <not @pattern Pattern>.
PCompound =
    / @rec <rec @label any @fields [Pattern ...]>
    / @arr <arr @items [Pattern ...]>
    / @dict <dict @entries { any: Pattern ...:... }> .

Template = TAttenuate / TRef / Lit / TCompound .
TAttenuate = <attenuate @template Template @attenuation [Caveat ...]>.
TRef = <ref @binding int>.
TCompound =
    / @rec <rec @label any @fields [Template ...]>
    / @arr <arr @items [Template ...]>
    / @dict <dict @entries { any: Template ...:... }> .
```

[Macaroons]: ../../glossary.md#macaroon
