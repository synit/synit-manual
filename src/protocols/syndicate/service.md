# Service dependencies

 - [`[syndicate-protocols]/schemas/service.prs`](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/service.prs)

The Service dependency management protocol allows programs to cooperate in declaring mutual
dependencies and scheduling subsystem startups, restarts, and shutdowns.

For an overview of service definitions and dependencies and a description of the available
assertions and messages, see [the operations manual](../../operation/service.md).

```
RequireService = <require-service @serviceName any>.
RunService = <run-service @serviceName any>.
RestartService = <restart-service @serviceName any>.

ServiceState = <service-state @serviceName any @state State>.
State = =started / =ready / =failed / =complete / @userDefined any .

ServiceDependency = <depends-on @depender any @dependee ServiceState>.
```
```
ServiceObject = <service-object @serviceName any @object any>.
```
