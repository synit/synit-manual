# Wire-protocol

 - [`[syndicate-protocols]/schemas/protocol.prs`](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/protocol.prs)

The wire-protocol schema, used for communication among entities separated by a point-to-point
link of some kind, is fully described as [part](../../protocol.md#packet-definitions) of [the
Syndicate Protocol specification](../../protocol.md).
