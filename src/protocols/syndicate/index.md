# Syndicated Actor Model schemas

 - Schema definitions: [`[syndicate-protocols]/schemas/`](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/)

The following pages document schemas associated with the [Syndicated Actor
Model](../../syndicated-actor-model.md). By and large, these schemas are contained in the
[syndicate-protocols Git
repository](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas).
