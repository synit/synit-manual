# Tracing

 - [`[syndicate-protocols]/schemas/trace.prs`](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/trace.prs)

See [the section on capturing and rendering interaction traces](../../guide/tracing.md) in the
reference manual.

Interaction traces are recorded as a sequence of `TraceEntry` records, each carrying a
`timestamp`, an `actor` identifier, and a trace `item`. Each `TraceEntry` describes a single
"activation" of an actor: an opportunity for it to run in response to some external event.

```
TraceEntry = <trace
              @timestamp @"seconds since Unix epoch" double
              @actor ActorId
              @item ActorActivation> .
ActorId = any .
```

## Activations and Turns

Activations come in three kinds: when the actor is *started* (and given its `actorName`); when
it takes a *turn* in response to some incoming event; and when it *stops* (with an exit
`status`).

```
ActorActivation =
/ <start @actorName Name>
/ @turn TurnDescription
/ <stop @status ExitStatus>
.

Name = <anonymous> / <named @name any> .
ExitStatus = =ok / protocol.Error .
```

A `TurnDescription` carries an `id`entifier, a `cause`, and a series of `actions` taken by the
active actor (that named by the `actor` field in the containing `TraceEntry`).

```
TurnDescription = <turn @id TurnId @cause TurnCause @actions [ActionDescription ...]> .
TurnId = any .
```

## Turn causes

A turn can be triggered for a number of reasons:

 - By some previous turn (`TurnCause.turn`); this may be because some actor sent the active
   party an event (a new assertion, a retraction, or a message), in which case the causing turn
   ID is the turn where the event originated, or because the active party has just been
   `spawn`ed by its parent, in which case the causing turn ID is the turn that performed the
   `spawn` action.

 - By the implicit cleanup process at the end of an actor's lifetime (`TurnCause.cleanup`). The
   actions in such turns are not under direct programmer control: instead, they embody the
   automatic retraction of assertions the actor has made that are still outstanding.

 - By termination of a [linked task](../../glossary.md#linked-task)
   (`TurnCause.linkedTaskRelease`).

 - By periodic activation (`TurnCause.periodicActivation`).

 - After a requested delay has expired (`TurnCause.delay`).

 - By some externally-arriving event outside the Syndicate model (`TurnCause.external`), such
   as an event from the operating system, available I/O, the "first cause" of a given program,
   and so on. Such causes have a free-form human readable `description` associated with them.

```
TurnCause =
/ @turn <caused-by @id TurnId>
/ <cleanup>
/ @linkedTaskRelease <linked-task-release @id TaskId @reason LinkedTaskReleaseReason>
/ @periodicActivation <periodic-activation @"`period` is in seconds" @period double>
/ <delay @causingTurn TurnId @"`amount` is in seconds" @amount double>
/ <external @description any>
.

LinkedTaskReleaseReason = =cancelled / =normal .
```

## Turn action descriptions

Turns include a sequence of `ActionDescription`s, each describing something that happened at
the active party during the turn:

 - `dequeue`: processing of a received `event`.
 - `enqueue`: enqueueing of a new `event` for delivery if and when the turn completes
   successfully.
 - `dequeueInternal`: processing an internally-queued event for one of the actor's own
   entities.
 - `enqueueInternal`: scheduling of an internal event for one of the actor's own entities.
 - `spawn`: creation of a new actor, identified by `id` and optionally scheduling creation of a
   [`link`](../../glossary.md#linked-actor) to its spawning actor.
 - `link`: a record of the actual creation of a [link](../../glossary.md#linked-actor) between
   a spawning and a spawned actor.
 - `facetStart` and `facetStop`: startup and shutdown of a [facet](../../glossary.md#facet)
   within the actor, identified by its `path`.
 - `linkedTaskStart`: creation of a [linked task](../../glossary.md#linked-task).

```
ActionDescription =
/ <dequeue @event TargetedTurnEvent>
/ <enqueue @event TargetedTurnEvent>
/ @dequeueInternal <dequeue-internal @event TargetedTurnEvent>
/ @enqueueInternal <enqueue-internal @event TargetedTurnEvent>
/ <spawn @link bool @id ActorId>
/ <link @parentActor ActorId
        @childToParent protocol.Handle
        @childActor ActorId
        @parentToChild protocol.Handle>
/ @facetStart <facet-start @path [FacetId ...]>
/ @facetStop <facet-stop @path [FacetId ...] @reason FacetStopReason>
/ @linkedTaskStart <linked-task-start @taskName Name @id TaskId>
.

FacetId = any .
TaskId = any .

FacetStopReason =
/ @explicitAction =explicit-action
/ =inert
/ @parentStopping =parent-stopping
/ @actorStopping =actor-stopping
.
```

## Event descriptions

Events sent, received, or processed by an actor are described with `TurnEvent` structures
describing each event. These are carried within `TargetedTurnEvent` structures, which add
information on the `target` of the event, and which are in turn contained in
`ActionDescription`s. Event `Target`s are triples of `actor` ID, `facet` ID, and entity `oid`.

```
TargetedTurnEvent = <event @target Target @detail TurnEvent> .
Target = <entity @actor ActorId @facet FacetId @oid Oid> .
Oid = any .
```

A `TurnEvent` is one of the core [types of event in the Syndicated Actor
Model](../../glossary.md#event):

 - `assert`: a new assertion, to be referred to later by its `handle`;
 - `retract`: a retraction of a previously-established assertion;
 - `message`: a simple message;
 - `sync`: a [synchronization event](../../glossary.md#synchronization); or
 - `breakLink`: a special kind of retraction notification that results from a broken
   [link](../../glossary.md#linked-actor) to another actor.

```
TurnEvent =
/ <assert @assertion AssertionDescription @handle protocol.Handle>
/ <retract @handle protocol.Handle>
/ <message @body AssertionDescription>
/ <sync @peer Target>
/ @breakLink <break-link @source ActorId @handle protocol.Handle>
.
```

Assertion and message bodies can be simple [Preserves](../../guide/preserves.md) values, or can
be some opaque, system-internal value, represented according to the system concerned.

```
AssertionDescription = <value @value any> / <opaque @description any> .
```
