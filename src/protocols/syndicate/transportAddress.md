# Transport addresses

 - [`[syndicate-protocols]/schemas/transportAddress.prs`](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/transportAddress.prs)

These utility definitions allow other schemas to discuss network transport addresses:

 - A `Tcp` address denotes a listening port on a particular host.
 - A `Unix` address denotes a UNIX domain socket by its filesystem path.
 - A `WebSocket` address contains a `ws://` or `wss://` URL.
 - A `Stdio` address denotes the standard input and output of a program.

```
Tcp = <tcp @host string @port int>.
Unix = <unix @path string>.
WebSocket = <ws @url string>.
Stdio = <stdio>.
```
