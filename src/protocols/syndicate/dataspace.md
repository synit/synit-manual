# "Observe" assertions

 - [`[syndicate-protocols]/schemas/dataspace.prs`](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/dataspace.prs)

The protocol for interaction with a [dataspace entity](../../glossary.md#dataspace) is very
simple: *any* assertion can be sent to a dataspace. The job of a dataspace is to relay
assertions on to interested peers; they do not generally interpret assertions themselves.

The sole exception is *assertions of interest in other assertions*.

These are called "Observe" assertions, or *subscriptions*:

```preserves-schema
Observe = <Observe @pattern dataspacePatterns.Pattern @observer #:any>.
```

An `Observe` assertion contains a [pattern](./dataspacePatterns.md) and a reference to an
observer entity. When an `Observe` assertion is published to a dataspace, the dataspace alters
its internal index to make a note of the new expression of interest. It also immediately relays
any of its already-existing assertions that match the pattern to the observer entity. As other
assertions come and go subsequently, the dataspace takes care to inform the observer entity in
the `Observe` record of the arrival or departure of any of the changing assertions that match
the pattern.
