# Building Up a System Layer
**or, Synit as a System Layer**

*Tony Garnock-Jones  
October 2022*

I will then design dataspace-based interaction protocols that realize
this functionality. These protocols will form the heart of the system:
each component will perform one or more roles as
described.

At the same time, the protocol descriptions will serve as internal and
external APIs and API documentation for the system layer. The
project’s thesis predicts that dataspace protocol descriptions will be
at the correct level to effectively capture the concepts intrinsic to
a system layer.

 - Protocols capturing a synthesis of system layer behaviours, based on the analysis



The theory of object
capabilities (“ocaps”), exemplified in languages such as E and
programming models such as Actors, offers a fine-grained approach that
can be made to scale further than a single machine. However, ocaps
only control access to shared programs. Access controls for shared
data are left implicit. In addition, ideas of location and system
boundary are left implicit in ocap systems.

I will adapt ocaps to syndicated actors. Because the Syndicated Actor model includes a
first-class notion of shared data as well as a layered conception of locations and location
boundaries, syndicated capabilities will reflect these ideas directly. I will generalize the
Syndicated Actor model’s existing notions of place, connecting capabilities not to individual
actors but to individual places and the data held therein. I will draw on existing ocap
literature, including in particular the recent notion of Macaroons ([Birgisson et al 2014][])
and older ideas from SPKI/SDSI ([Ylonen et al 1999][]; [Ellison 1999][]).


**Q. How do you feel dataspaces would most enhance privacy or trust?**

Capability technology offers strong, flexible control over access to any given dataspace
without getting lost in the weeds of identity management: identity is an application-local,
application-private concern.

Dataspaces default to being closed, "invite-only" networks, meaning casual observation of
activity in a dataspace is not possible. But the necessary extension of the capability model to
handle the data-sharing aspects of dataspaces gives benefit in terms of privacy and trust that
goes beyond the already considerable benefits a traditional capability model offers.

Traditional capabilities directly control access to behavioural objects, and only indirectly
control access to data held within such objects. Syndicated capabilities, by contrast, directly
control access to shared data held within a space - changes to which may trigger activity in
"objects" participating in the dataspace.

In other words, traditional capabilities encode data access controls in terms of object access
controls; syndicated capabilities, vice versa.

This ability to directly express access to shared data gives system designers a powerful tool
for thinking about permitted information flows, including questions of privacy. Furthermore,
*attenuating* the authority of syndicated capabilities before passing them on to some other
principal allows for strong partitioning of access within a dataspace, offering fine-grained,
local, compositional decisions about access to shared data. Finally, it becomes possible to
expose capabilities to end-users (roughly analogous to URLs), putting that power in their hands
also.

I should also mention that dataspaces can scale from managing activity within a single OS
process up to coordinating activity between machines around the world. A distributed dataspace
could be an excellent foundation for collaborative applications, where privacy concerns come to
the forefront. In effect, a dataspace can become a richly-structured "VPN", containing
application-specific shared data and with application- or schema-specific access controls.

## References

[Birgisson et al 2014]: #ref:birgisson14
[**Birgisson et al 2014**] <span id="ref:birgisson14"> Birgisson, Arnar, Joe Gibbs Politz,
Úlfar Erlingsson, Ankur Taly, Michael Vrable, and Mark Lentczner. “Macaroons: Cookies with
Contextual Caveats for Decentralized Authorization in the Cloud.” In Network and Distributed
System Security Symposium. San Diego, California: Internet Society, 2014.</span>

[Ellison 1999]: #ref:ellison99
[**Ellison 1999**] <span id="ref:ellison99"> Ellison, Carl. SPKI Requirements. Request for
Comments 2692. RFC Editor, 1999. <https://doi.org/10.17487/RFC2692>.</span>

[Ylonen et al 1999]: #ref:ylonen99
[**Ylonen et al 1999**] <span id="ref:ylonen99"> Ylonen, Tatu, Brian Thomas, Butler Lampson,
Carl Ellison, Ronald L. Rivest, and William S. Frantz. SPKI Certificate Theory. Request for
Comments 2693. RFC Editor, 1999. <https://doi.org/10.17487/RFC2693>.</span>
