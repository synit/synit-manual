# Source code, Building, and Installation

The initial application of Synit is to *mobile phones*.

As such, in addition to regular [system layer](./glossary.md#system-layer) concepts, Synit
supports concepts from mobile telephony: calls, SMSes, mobile data, headsets, speakerphone,
hotspots, battery levels and charging status, and so on.

Synit builds upon many existing technologies, but primarily relies on the following:

 - [PostmarketOS](https://postmarketos.org/). Synit builds on PostmarketOS, replacing only a
   few core packages. All of PostmarketOS and Alpine Linux are available underneath Synit.

 - [Preserves](https://preserves.dev/). The Preserves data language and its associated
   schema and query languages are central to Synit.

 - [Syndicate](https://syndicate-lang.org/). Syndicate is an umbrella project for tools and
   specifications related to the [Syndicated Actor Model](./glossary.md#syndicated-actor-model)
   (the SAM).

## You will need

 - A Linux development system. (I use Debian testing/unstable.)

 - Rust nightly and Cargo (perhaps installed via [rustup](https://rustup.rs/)).

 - The rust `cross` tool (even for `x86_64` builds):

   ```
   cargo install cross
   ```

 - Docker (containers are used frequently for building packages, among other things!)

 - Python 3.9 or greater

 - `git`, `ssh`, `rsync`

 - Make, a C compiler, and so on; standard Unix programming tools.

 - The `preserves-tool` binary installed on your `PATH`:

   ```
   cargo install preserves-tools
   ```

 - <a id="binfmt"></a>`qemu` and its `binfmt` support (even for `x86_64` builds). On Debian,
   `apt install binfmt-support qemu-user-static`.[^debian-qemu-bug]

 - Source code for Synit components (see below).

 - A standard PostmarketOS distribution for the target computer or mobile phone. If you don't
   want to install on actual hardware, you can use a virtual machine. See the [instructions for
   installing PostmarketOS](https://wiki.postmarketos.org/wiki/Installation_guide).

 - **Great tolerance for the possibility of soft-bricking your phone.** This is experimental
   software! When it breaks, you'll often have to (at least) reinstall PostmarketOS from
   absolute scratch on the machine. I do lots of development using `qemu-amd64` for this
   reason. See [here](./emulator.md) for [instructions on running Synit on a emulated
   device](./emulator.md).

Here's a small shell snippet to quickly check for the dependencies you will need:[^contact-if-missing]

```shell
(
    rustc +nightly --version
    cross +nightly --version
    docker --version
    python3 --version
    git --version
    ssh -V 2>&1
    rsync --version | head -1
    make --version | head -1
    cc --version | head -1
    preserves-tool --version
    qemu-system-aarch64 --version | head -1
    ls -la /proc/sys/fs/binfmt_misc/qemu-aarch64 2>&1
) 2>/dev/null
```

On my machine, it outputs:

```
rustc 1.78.0-nightly (878c8a2a6 2024-02-29)
cross 0.2.5
cargo 1.78.0-nightly (8964c8ccf 2024-02-27)
Docker version 20.10.25+dfsg1, build b82b9f3
Python 3.11.8
git version 2.43.0
OpenSSH_9.6p1 Debian-4, OpenSSL 3.1.5 30 Jan 2024
rsync  version 3.2.7  protocol version 31
GNU Make 4.3
cc (Debian 13.2.0-13) 13.2.0
preserves-tool 4.994.0
QEMU emulator version 8.2.1 (Debian 1:8.2.1+ds-2)
-rw-r--r-- 1 root root 0 Mar  1 15:37 /proc/sys/fs/binfmt_misc/qemu-aarch64
```

## Get the code

The Synit codebase itself is contained in the [`synit` git
repository](https://git.syndicate-lang.org/synit/synit):

    git clone https://git.syndicate-lang.org/synit/synit

See the [README](https://git.syndicate-lang.org/synit/synit/src/branch/main/README.md) for an
overview of the contents of the repository.

Synit depends on published packages for Preserves and Syndicate support in each of the many
programming languages it uses. These will be automatically found and downloaded during the
Synit build process, but you can find details on the [Preserves](https://preserves.dev/)
and [Syndicate](https://syndicate-lang.org/) homepages, respectively.

For the Smalltalk-based phone-management and UI part of the system, you will need a number of
other tools. See the
[README](https://git.syndicate-lang.org/tonyg/squeak-phone/src/branch/main/README.md) for the
`squeak-phone` repository:

    git clone https://git.syndicate-lang.org/tonyg/squeak-phone

## Build the packages

To build, type `make ARCH=`*\<architecture\>* in the `packaging` directory of your checkout,
where *\<architecture\>* is one of:

 - `aarch64` (default), for e.g. Pinephone or Samsung Galaxy S7 deployment
 - `x86_64`, for e.g. `qemu-amd64` deployment

If you see errors of the form "`exec /bin/sh: exec format error`" while building, say, the
`aarch64` packages using an `x86_64` build host, you need to install qemu's binfmt support. See
[above](#binfmt).

The result of the build will be a collection of Alpine Linux `apk` packages in
`packaging/target/packages/`*\<architecture\>*`/`. At the time of writing, these include

 - `preserves-schemas`, common schema files for working with general Preserves data and schemas
 - `preserves-tools`, standard command-line tools for working with Preserves documents
   (pretty-printer, document query processor, etc.)
 - `py3-preserves`, python support libraries for Preserves
 - `py3-syndicate`, python support for the Syndicated Actor Model
 - `squeak-cog-vm` and `squeak-stack-vm`, Squeak Smalltalk virtual machine for the
   Smalltalk-based portions of the system
 - `syndicate-schemas`, common schema files for working with the Syndicated Actor Model
 - `syndicate-server`, package for the core system bus
 - `synit-config`, main package for Synit, with configuration files, `init` scripts, system
   daemons and so on.
 - `synit-pid1`, PID1 program for Synit that starts the core system bus and then becomes
   passive

## Install PostmarketOS on your system

Follow the instructions for your device on the [PostmarketOS
wiki](https://wiki.postmarketos.org/wiki/Devices).

Boot and connect your device to your development machine. Make sure you can `ssh` into it.

## Upload Synit packages

Change to the `scripts/` directory, and run the `./upload-bundle.sh` script from there to rsync
the ingredients for transforming stock PostmarketOS to Synit to the phone.

## Run the transformation script

Use `ssh` to log into your phone. Run `./transmogrify.sh`. (If your user's password on the
phone is anything other than `user`, you will have to run `SUDOPASS=yourpassword
./transmogrify.sh`.)

This will install the Synit packages. After this step is complete, next time you boot the
system, it will boot into Synit. It may very well be unbootable at this point, depending on the
state of the codebase! Make sure you know how to restore to a stock PostmarketOS installation.

## Install the Smalltalk parts of the system (optional)

If you want to experiment with the Smalltalk-based modem support and UI, follow the
instructions in the [squeak-phone
README](https://git.syndicate-lang.org/tonyg/squeak-phone/src/branch/main/README.md) now.

## Reboot and hope

With luck, you'll see the Smalltalk user interface start up. (If you didn't install the UI, you
should still be able to `ssh` into the system.) From here, you can operate the system normally,
following the information in [the following chapter](./operation/index.md).

---

#### Notes

[^debian-qemu-bug]: Version `1:7.0+dfsg-7` of `qemu-user-static` has a bug (possibly [this
    one](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1014177)) which makes Docker-based
    cross builds hang. *Downgrading* `qemu-user-static` to version `1:5.2+dfsg-11+deb11u2`
    worked for me, as did *upgrading* (as of October 2022) to version `1:7.1+dfsg-2`.

[^contact-if-missing]: Please [contact me at
    `tonyg@leastfixedpoint.com`](mailto:tonyg@leastfixedpoint.com) if a dependency needs to be
    added to the list.
