# Summary

[Introduction](./introduction.md)

# Preliminaries

- [Architecture](./architecture.md)
- [Source code, Building, and Installation](./install.md)
- [Running on an emulated device](./emulator.md)
- [Glossary](./glossary.md)

# User and Administration Guide

- [System overview](./operation/index.md)
- [The System Bus: syndicate-server](./operation/system-bus.md)
  - [Configuration language](./operation/scripting.md)
  - [Services and service dependencies](./operation/service.md)
  - [Built-in services and service classes](./operation/builtin/index.md)
    - [Gatekeeper](./operation/builtin/gatekeeper.md)
    - [Logging](./operation/builtin/logging.md)
    - [TCP/IP, WebSocket and Unix-socket Transports](./operation/builtin/relay-listener.md)
    - [Configuration watcher](./operation/builtin/config-watcher.md)
    - [Daemons and external programs](./operation/builtin/daemon.md)
- [Configuration files and directories](./operation/synit-config.md)
- [How-to ...](./operation/howto/index.md)
  - [Define services and service classes](./operation/howto/define-services.md)
  - [Restart services](./operation/howto/restart-services.md)
  - [Schedule one-off or repeating tasks](./operation/howto/schedule-tasks.md)
  - [Manage user settings](./operation/howto/manage-user-settings.md)
  - [Reboot and power off the machine](./operation/howto/reboot-and-power-off.md)
  - [Suspend the machine](./operation/howto/suspend.md)

# Tools

- [The `preserves-tools` package](./tools/preserves-tools.md)

# Programming Guide and Reference

- [Preserves](./guide/preserves.md)
- [Working with schemas](./guide/working-with-schemas.md)
- [Capturing and rendering interaction traces](./guide/tracing.md)

# Programming Libraries

- [Python support libraries](./libraries/python-support.md)
- [Shell-scripting libraries](./libraries/shell-scripting.md)

# Protocols and Schema Definitions

- [Preserves schemas](protocols/preserves/index.md)
  - [Preserves Schema metaschema](protocols/preserves/schema.md)
  - [Preserves Path schema](protocols/preserves/path.md)
- [Syndicated Actor Model schemas](protocols/syndicate/index.md)
  - ["Observe" assertions](protocols/syndicate/dataspace.md)
  - [Patterns over assertions](protocols/syndicate/dataspacePatterns.md)
  - [Gatekeeper and Sturdy-references](protocols/syndicate/gatekeeper.md)
  - [Wire-protocol](protocols/syndicate/protocol.md)
  - [Service dependencies](protocols/syndicate/service.md)
  - [Tracing](protocols/syndicate/trace.md)
  - [Transport addresses](protocols/syndicate/transportAddress.md)
- [Synit schemas](protocols/synit/index.md)
  - [Audio control](protocols/synit/audio.md)
  - [Telephony (call and SMS) support](protocols/synit/telephony.md)
  - [Modem support](protocols/synit/modem.md)
  - [MIME type support](protocols/synit/mime.md)
  - [Network core status and configuration](protocols/synit/network.md)
  - [Sound effects](protocols/synit/soundEffects.md)
  - [Time stamps](protocols/synit/time.md)
  - [User interface definitions and interaction](protocols/synit/ui.md)
  - [User settings](protocols/synit/userSettings.md)

# Specifications and Theory

- [Syndicated Actor Model](./syndicated-actor-model.md)
- [Protocol specification](./protocol.md)
- [Breaking Down the System Layer](./system-layer.md)
<!-- - [Building Up a System Layer](./synit-as-system-layer.md) -->
