# Architecture

<!--

> What is an architecture? It's the rules of the platform which are
> true on any embodiment of it, not just the one you work on today.
> These rules are aimed at programs (what are valid programs) and the
> design (its invariants) both. — [Olivier
> Giroux](https://twitter.com/__simt__/status/1489790925266059264),
> Feb. 2022

-->

The **[Syndicated Actor Model (SAM)](./glossary.md#syndicated-actor-model)** is at the heart of
Synit. In turn, the SAM builds upon [E-style actors](./glossary.md#e), replacing
message-exchange with eventually-consistent *state replication* as the fundamental building
block for interaction. Both E and the SAM are instances of the [Object Capability (ocap)
model](./glossary.md#object-capability-model), a [compositional](./glossary.md#compositional)
approach to system security.

The "feel" of the system is somewhere between Smalltalk-style
object-orientation, publish-subscribe programming, E- or Erlang-style
actor interaction, Prolog-style logic programming, and Esterel-style
reactive dataflow.

 1. **Programs are Actors.** Synit *programs*
    ("[actors](./glossary.md#actor)" in the SAM) interoperate by
    *[dataspace](./glossary.md#dataspace)*-mediated exchange of
    *[messages](./glossary.md#message)* and replication of
    *[conversational state](./glossary.md#conversational-state)*
    expressed as *[assertions](./glossary.md#assertion)*.

 2. **Ocaps for security and privacy.** The ocap model provides the fundamental building blocks
    for secure composition of programs in the system. Synit extends the core ocap model with
    [Macaroon](./glossary.md#macaroon)-inspired *[attenuation](./glossary.md#attenuation)* of
    capabilities, for both limiting visibility of state and constraining access to behaviour.

 3. **Reactivity and homeostasis.** Programs publish relevant aspects of their internal state
    to peers (usually by placing [assertions](./glossary.md#assertion) in a dataspace). Peers
    [subscribe](./glossary.md#observe) to those assertions, reacting to changes in state to
    preserve overall system equilibrium.

 3. **Heterogeneous; "open".** Different programming languages and styles interoperate freely.
    Programs may or may not be structured internally using SAM principles: the system as a
    whole is where the architectural principles are applied. However, it often makes good sense
    to use SAM principles *within* a given Synit program as well as *between* programs.

 4. **Language-neutral.** Where possible, programs interoperate via a
    simple [protocol](./protocol.md) across transports like TCP/IP,
    WebSockets, and Unix sockets and pipes. Otherwise, they
    interoperate using traditional Unix techniques. The concrete
    syntax for the messages and assertions exchanged among programs is
    the [Preserves](./guide/preserves.md) data language.

 5. **Strongly typed.** Preserves [Schemas](./guide/preserves.md#schemas)
    describe the data exchanged among programs. Schemas compile to
    type definitions in various programming languages, helping give an
    ergonomic development experience as well as ensuring safety at
    runtime.
