# How to manage user settings

Send a [`user-settings-command`](../../protocols/synit/userSettings.md) message containing an
`assert` or `retract` record containing the setting assertion to add or remove. Use the [`!`
operator of the configuration language](../scripting.md#SendInstruction) to send a message (as
opposed to make an assertion):

```preserves
! <user-settings-command <assert <mobile-data-enabled>>>
! <user-settings-command <assert <mobile-data-apn "internet">>>
! <user-settings-command <retract <mobile-data-enabled>>>
```

In future, a command-line tool for sending such messages will be provided; for now, create
temporary configuration language scripts in `/run/etc/syndicate/services`:

```shell
THROCK=/run/etc/syndicate/services/throck.pr
echo '! <user-settings-command <assert <mobile-data-enabled>>>' > $THROCK
sleep 1
rm -f $THROCK
```
