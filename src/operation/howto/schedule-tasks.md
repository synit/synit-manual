# How to schedule one-off or repeating tasks

(TODO. Not yet implemented: a cron-like program will eventually respond to assertions demanding
periodic or delayed execution of tasks (likely expressed as assertions, making it more of a
delayed-or-periodic-assertion-producing program).)

## Timer tolerances

Apple has come up with the useful idea of a [timer tolerance][apple-timer-tolerance],
applicable to both repeating and one-off timers. In [their
documentation][apple-timer-tolerance], they write:

> The timer may fire at any time between its scheduled fire date and the scheduled fire date
> plus the tolerance. [...] A general rule, set the tolerance to at least 10% of the interval
> [...] Even a small amount of tolerance has significant positive impact on the power usage of
> your application.

[apple-timer-tolerance]: https://developer.apple.com/documentation/foundation/timer#1667624

## One-off tasks
## Repeating tasks
