# How to reboot and power off the machine

(TODO. Not yet implemented: eventually, `synit-pid1` will respond to messages/assertions from
the dataspace, implementing the necessary coordination for a graceful shutdown procedure. For
now, `sync` three times, sleep a bit, and `reboot -f` or `poweroff -f`...)
