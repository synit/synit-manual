# How to restart services

Send a [`restart-service`](../service.md#restart-service) message mentioning the [service
name](./define-services.md#service-names) of the service to restart. Use the [`!` operator of
the configuration language](../scripting.md#SendInstruction) to send a message (as opposed to
make an assertion):

```preserves
! <restart-service <daemon <wifi-daemon "wlan0">>>
! <restart-service <daemon user-settings-daemon>>
```

In future, a command-line tool for sending messages to a system dataspace will be provided; for
now, create temporary configuration language scripts in `/run/etc/syndicate/services`:

```shell
THROCK=/run/etc/syndicate/services/throck.pr
echo '! <restart-service <daemon <wifi-daemon "wlan0">>>' > $THROCK
sleep 1
rm -f $THROCK
```
