# The System Bus: syndicate-server

 - Source code: [`[syndicate-rs]/syndicate-server/`](https://git.syndicate-lang.org/syndicate-lang/syndicate-rs/src/branch/main/syndicate-server)
 - Packaging: [`[synit]/packaging/packages/syndicate-server/`](https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/syndicate-server)

The `syndicate-server` program has a number of closely-related functions. In many ways, it is a
reification of the [system layer concept](../glossary.md#system-layer) itself.

It provides:

 1. A **[root system bus](#the-root-system-bus)** service for use by other programs. In this way, it is
    analogous to D-Bus.

 2. A **[configuration language](./scripting.md)** suitable for programming
    [dataspaces](../glossary.md#dataspace) with simple reactive behaviours.

 3. A general-purpose **[service dependency tracking facility](./service.md)**.

 4. A [**gatekeeper** service](./builtin/gatekeeper.md), for exposing
    [capabilities](../glossary.md#capability) to running objects as (potentially long-lived)
    [macaroon](../glossary.md#macaroon)-style "sturdy references", plus TCP/IP- and
    Unix-socket-based **[transports](./builtin/relay-listener.md)** for accessing capabilities
    through the gatekeeper.

 5. An [`inotify`](https://en.wikipedia.org/wiki/Inotify)-based **[configuration
    tracker](./builtin/config-watcher.md)** which loads and executes configuration files written
    in the scripting language.

 6. [**Process startup and supervision**](./builtin/daemon.md)
    services for running external programs.

The program can also be used as an "inferior" bus. For example, there may be a per-user bus, or
a per-session bus, or both. Each bus would appropriately scope the lifetime of its supervised
processes.

Finally, it can be used completely standalone, outside a Synit context.

## The root system bus

The [`synit-pid1`](./index.md#sbinsynit-pid1) program invokes `syndicate-server` like this:

    /usr/bin/syndicate-server --inferior --config /etc/syndicate/boot

The first flag, `--inferior`, tells the server to expect to be able to communicate on its
stdin/stdout using the [standard wire protocol](../protocol.md). This lets `synit-pid1` join
the community of actors running within the system dataspace.

The second flag, `--config /etc/syndicate/boot`, tells the server to start monitoring the
directory tree rooted at `/etc/syndicate/boot` for changes. Files whose names end with `.pr`
within that tree are loaded as configuration script files.

Almost all of Synit is a consequence of careful use of the configuration script files in
`/etc/syndicate`.
