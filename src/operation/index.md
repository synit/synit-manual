# System overview

Synit uses the Linux kernel as a hardware abstraction and virtualisation layer.

All processes in the system are arranged into a [supervision
tree](../glossary.md#supervision-tree), conceptually rooted at the [system
bus](./system-bus.md).

```ditaa system-supervision-tree
              +----------------------------------+
              |Root System Bus (syndicate–server)|
              +----------------+-----------------+
                               |
   +--------+--------+---------+----------+---------------+
   |        |        |         |          |               |
+--+--+ +---+---+ +--+--+ +----+----+ +---+---+     +-----+-----+
|init | |console| |udevd| |Network  | |Wifi   |     |Session bus|
+-----+ |getty  | +-----+ |Interface| |Daemon | ... |(syndicate–|
        +-------+         |Monitor  | |(wlan0)|     |  server)  |
                          |Daemon   | +-------+     +-----+-----+
                          +---------+                     |
                                                          |
                  +----------+----------------------------+
                  |          |                            |
              +---+---+   +--+--+                    +----+---+
              |Browser|   |Email|       . . .        |X Server|
              +-------+   +-----+                    +--------+

```

While `init` is PID 1, and thus the root of the tree of processes according to the kernel, it
is *not* the root of the supervision tree. The `init` process, acting as management daemon for
the *kernel* from Synit's perspective, is "supervised" by the system bus like all other
services. The supervision tree is a Synit concept, not a Linux concept.

<!--
Here's an example of `ps` output from a Synit prototype running on a mobile phone:

      PID TTY      STAT   TIME COMMAND
        1 ?        Ssl    0:00 /sbin/synit-pid1
     1034 ?        Sl     0:00 /usr/bin/syndicate-server --inferior --config /etc/syndicate/boot
     1045 ttyS0    Ss+    0:00  \_ getty 0 /dev/console
     1052 ?        S      0:01  \_ /sbin/udevd --children-max=5
     1113 ?        Sl     0:11  \_ python3 /usr/lib/synit/interface-monitor
     1129 ?        S      0:00  \_ sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
     1286 ?        S      0:00  \_ wpa_supplicant -Dnl80211,wext -C/run/wpa_supplicant -i wlan0
     1287 ?        S      0:00  \_ udhcpc -i usb0 -fR -s /usr/lib/synit/udhcpc.script
     1390 ?        S      0:00  \_ ntpd -d -n -p pool.ntp.org
     1431 ?        S      0:01  \_ python3 /usr/lib/synit/user-settings-daemon
     1497 ?        Sl     0:01  \_ python3 /usr/lib/synit/wifi-daemon
     1516 ?        S      0:00  \_ udhcpc -i wlan0 -fR -s /usr/lib/synit/udhcpc.script
     1035 ?        S      0:00 s6-log t s999999 n500 /var/log/synit
-->

## Boot process

The kernel first loads the stock PostmarketOS `initrd`, which performs a number of important
tasks and then delegates to `/sbin/init`.

### /sbin/init = synit-init.sh

 - Source code: [`[synit]/packaging/packages/synit-config/files/sbin/synit-init.sh`](https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-config/files/sbin/synit-init.sh)

The [`synit-config` package](../install.md#build-the-packages) overrides the usual contents of
`/sbin/init`, replacing it with a short shell script, `synit-init.sh`. This script, in turn,
takes care of a few boring tasks such as mounting `/dev`, `/proc`, `/run`, etc., ensuring that
a few important directories exist, and remounting `/` as read-write before `exec`ing
`/sbin/synit-pid1`.

For the remainder of the lifetime of the system, `/sbin/synit-pid1` is the PID 1 `init`
process.

### /sbin/synit-pid1

 - Source code: [`[synit]/synit-pid1/`](https://git.syndicate-lang.org/synit/synit/src/branch/main/synit-pid1)
 - Packaging: [`[synit]/packaging/packages/synit-pid1/`](https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-pid1)

The `synit-pid1` program starts by spawning the [system bus](./system-bus.md)
(`syndicate-server` in the process tree above) and the program `/sbin/synit-log`, connecting
`stderr` of the former to `stdin` of the latter.

It then goes on to perform two tasks concurrently: the first is the Unix
[`init`](https://en.wikipedia.org/wiki/Init) role, reaping zombie processes, and the second is
to interact with the system bus as an ordinary system service.

The latter allows the system to treat `init` just like any other part of the system, accessing
its abilities to reboot or power off the system using messages and assertions in the system
dataspace as usual.

Even though `synit-pid1` is, to the kernel, a *parent* process of `syndicate-server`, it is
logically a *child* process.

### /sbin/synit-log

 - Source code: [`[synit]/packaging/packages/synit-pid1/synit-log`](https://git.syndicate-lang.org/synit/synit/src/branch/main/packaging/packages/synit-pid1/synit-log)

This short shell script invokes the [S6](../glossary.md#s6) program `s6-log` to capture log
output from the system bus, directing it to files in `/var/log/synit/`.
