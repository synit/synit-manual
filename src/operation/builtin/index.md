# Built-in services and service classes

The `syndicate-server` program includes built-in knowledge about a handful of useful services,
including a means of loading external programs and integrating them into the running system.

 - Every server program starts a [**gatekeeper**](./gatekeeper.md) service, which is able to
   manage conversion between [live references](../../glossary.md#reference) and so-called
   "sturdy refs", long-lived capabilities for access to resources managed by the server.

 - A simple [**logging**](./logging.md) actor copies log messages from the system dataspace to
   the server's standard error file descriptor.

 - Any number of [**TCP/IP, WebSocket, and Unix socket transports**](./relay-listener.md) may
   be configured to allow external access to the gatekeeper and its registered services. (These
   can also be started from the `syndicate-server` command-line with `-p` and `-s` options.)

 - Any number of [**configuration watchers**](./config-watcher.md) may be created to monitor
   directories for changes to files written using the [server scripting
   language](../scripting.md). (These can also be started from the `syndicate-server`
   command-line with `-c` options.)

 - Finally, [**external programs**](./daemon.md) can be started, either as long-lived "daemon"
   services or as one-off scripts.

## Resources available at startup

The `syndicate-server` program uses the Rust
[`tracing`](https://docs.rs/tracing/latest/tracing/) crate, which means different levels of
internal logging verbosity are available via the `RUST_LOG` environment variable. [See here for
more on `RUST_LOG`](https://docs.rs/env_logger/latest/env_logger/#enabling-logging).

If tracing of [Syndicated Actor Model actions](../../glossary.md#action) is enabled with the
`-t` flag, it is configured prior to the start of the main server actor.

As the main actor starts up, it

 - creates a fresh dataspace, known as the `$config` dataspace, intended to contain
   top-level/global configuration for the server instance;

 - creates a fresh dataspace, known as `$log`, for assertions and messages related to service
   logging within the server instance;

 - creates the `$gatekeeper` actor implementing the [gatekeeper](./gatekeeper.md) service,
   attaching it to the `$config` dataspace;

 - exposes `$config`, `$log` and `$gatekeeper` as the variables available to configuration
   scripts loaded by [config-watchers](./config-watcher.md) started with the `-c` flag (N.B.
   the `$config` dataspace is thus the default target for assertions in config files);

 - creates service factories monitoring various [service assertions](../service.md) in the
   `$config` dataspace;

 - processes `-p` command-line options, each of which creates a [TCP/IP relay
   listener](./relay-listener.md#tcpip);

 - processes `-s` command-line options, each of which creates a [Unix socket relay
   listener](./relay-listener.md#unix-sockets);

 - processes `-c` command-line options, each of which creates a
   [config-watcher](./config-watcher.md) monitoring a file-system directory; and finally

 - creates the [logging actor](./logging.md), listening to certain events on the `$log`
   dataspace.

Once these tasks have been completed, it quiesces, leaving the rest of the operation of the
system up to other actors (relay-listeners, configuration scripts, and other configured
services).
