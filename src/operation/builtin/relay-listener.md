# Relay Listeners

 - Relevant schema:
    - [[syndicate-rs]/syndicate-server/protocols/schemas/internalServices.prs](https://git.syndicate-lang.org/syndicate-lang/syndicate-rs/src/branch/main/syndicate-server/protocols/schemas/internalServices.prs)
    - [[syndicate-protocol]/schemas/transportAddress.prs](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/transportAddress.prs)

The `syndicate-server` program can be configured to listen on TCP/IP ports and Unix
sockets[^sock-stream-only] for incoming connections speaking the [Syndicate network
protocol][protocol].

[protocol]: ../../protocol.md

## <span id="tcpip"></span>TCP/IP and WebSockets

Assertions [requiring](../service.md#require-service) a service with name matching
`TcpRelayListener` cause the server to start a TCP server socket on the given `addr`'s `host`
and `port`, exposing the `gatekeeper` [entity reference](../../glossary.md#reference) as the
[initial ref](../../glossary.md#initial-ref) of incoming connections:

```preserves-schema
TcpRelayListener = <relay-listener @addr Tcp @gatekeeper #:gatekeeper.Resolve> .
Tcp = <tcp @host string @port int>.
```

When a new connection arrives, the first byte is examined to see what kind of connection it is
and which Preserves syntax it will use.

 - If it is ASCII "`G`" (0x47), it cannot be the start of a [protocol][] packet, so it is
   interpreted as the start of a WebSocket connection and handed off to the
   [tokio_tungstenite](https://docs.rs/tokio-tungstenite/latest/tokio_tungstenite/) WebSocket
   library. Within the WebSocket's context, each packet must be encoded as a binary packet
   using Preserves binary syntax.

 - Otherwise, if it could start a valid UTF-8 sequence, the connection will be a plain TCP/IP
   link using the Preserves text syntax.

 - Otherwise, it's a byte which cannot be part of a valid UTF-8 sequence, so it is interpreted
   as a Preserves binary syntax tag: the connection will be a plain TCP/IP link using Preserves
   binary syntax.

## Unix sockets

Assertions [requiring](../service.md#require-service) a service with name matching
`UnixRelayListener` cause the server to start a Unix server socket on the given `addr`'s
`path`, exposing the `gatekeeper` [entity reference](../../glossary.md#reference) as the
[initial ref](../../glossary.md#initial-ref) of incoming connections:

```preserves-schema
UnixRelayListener = <relay-listener @addr Unix @gatekeeper #:gatekeeper.Resolve> .
Unix = <unix @path string>.
```

Syntax autodetection is as for [TCP/IP](#tcpip), except that WebSockets are not supported over
Unix sockets.

----

#### Notes

[^sock-stream-only]: Only `SOCK_STREAM` Unix sockets are supported, at present. In future,
    `SOCK_DGRAM` could be useful for e.g. file-descriptor passing.
