# Configuration watcher

 - Relevant schema: [[syndicate-rs]/syndicate-server/protocols/schemas/internalServices.prs](https://git.syndicate-lang.org/syndicate-lang/syndicate-rs/src/branch/main/syndicate-server/protocols/schemas/internalServices.prs)

Assertions [requiring](../service.md#require-service) a service with name matching
`ConfigWatcher` cause the server to start a configuration watcher service monitoring files in
and subdirectories of the given `path` for changes:

```preserves-schema
ConfigWatcher = <config-watcher @path string @env ConfigEnv>.
ConfigEnv = { symbol: any ...:... }.
```

The `path` may name either a file or directory. Any time the configuration watcher finds a file
matching the glob `*.pr` within the tree rooted at `path`, it loads the file. Each time a
`*.pr` file is loaded, it is interpreted as a [configuration scripting
language](../scripting.md) program, with a copy of `env` as the "initial environment" for the
script.

Whenever a change to a `*.pr` file is detected, the configuration watcher reloads the file,
discarding previous internal state related to the file.

Note that a quirk of the config language requires that there exist an entry in `env` with key
the symbol `config` and value an [entity reference](../../glossary.md#reference) (usually
denoting a [dataspace entity](../../glossary.md#dataspace)). However, the `config` entry need
not be the same as the surrounding `$config`! A useful pattern is to set up a new
`ConfigWatcher` with `env` containing a `config` binding pointing to an
[attenuated](../../glossary.md#attenuation) reference to the current `config` dataspace, or
even [an entirely fresh dataspace](../scripting.md#expr:dataspace) created specifically for the
task.
