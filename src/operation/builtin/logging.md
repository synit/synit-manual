# Logging

The Synit logging infrastructure is still underdeveloped.

At present, there is an actor created at `syndicate-server` startup time that monitors the
`$log` dataspace for messages of the form:

```preserves-schema
LogEntry = <log @timestamp string @detail { any: any ...:... }> .
```

When it receives a log entry, it looks for a few conventional and optional keys in the `detail`
field, each permitted to be any kind of [value](../../glossary.md#value):

 - `pid`, conventionally a Unix process ID;
 - `line`, conventionally a string of free-form text intended for people to read;
 - `service`, conventionally a service name in the sense of
   [`require-service`](../service.md#require-service)/[`run-service`](../service.md#run-service); and
 - `stream`, conventionally one of the symbols `stdout` or `stderr`.

The timestamp and the special keys are then formatted, along with all other information in the
entry record, and printed to the `syndicate-server`'s standard error at `INFO` level using
[`tracing`](https://docs.rs/tracing/latest/tracing/).
