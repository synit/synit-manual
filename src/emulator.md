# How to get Synit running on an emulated PostmarketOS device

[vouch.id]: https://vouch.id/

Begin by following the [generic PostmarketOS instructions for running using
QEMU](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)), reprised here briefly, and
then build and install the Synit packages and (optionally) the SqueakPhone user interface.

## Build and install `qemu-amd64` PostmarketOS

First, run `pmbootstrap init` (choose `qemu`, `amd64`, and a `console` UI); or, if you've done
that previously, run `pmbootstrap config device qemu-amd64`.

Then, run `pmbootstrap install` to build the rootfs.

Finally, run `pmbootstrap qemu --video 720x1440@60` to (create, if none has previously been
created, and) start an emulated PostmarketOS device. You'll run that same command each time you
boot up the machine, so create an alias or script for it, if you like.

## Set up `ssh` access to the emulated device

I have the following stanza in my `~/.ssh/config`:

```
Host pm-qemu
    HostName localhost
    Port 2222
    User user
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
```

Log in to the device using a username and password (`SSH_AUTH_SOCK= ssh pm-qemu`) and set up
SSH key access via `~/.ssh/authorized_keys` on the device, however you like to do it. I use
[vouch.id][] to log into my machines using an SSH certificate, so I do the
following:

```
mkdir -p .local/bin
cd .local/bin
wget https://vouch.id/download/vouch
chmod a+x vouch
sudo apk add python3
echo 'export VOUCH_ID_PRINCIPAL=tonyg@leastfixedpoint.com' >> ~/.profile
```

Then I log out and back in again to pick up the `VOUCH_ID_PRINCIPAL` variable, followed by
running

```
vouch server setup --accept-principals tonyg
```

(Substitute your own preferred certificate principal username, of course.) After this, I can
use the [vouch.id][] app to authorize SSH logins.

## Allow port forwarding over SSH to the device

Edit `/etc/ssh/sshd_config` to have `AllowTcpForwarding yes`. This will let you use e.g.
port-forwarded VNC over your SSH connection to the device once you have the user interface set
up.

## Build and install the Synit packages

Follow the [build and installation instructions](./install.md) to check out and build the code.

Once you've checked out the `synit` module and have all the necessary build dependencies
installed, change directory to `synit/packaging/squid` and run `start.sh` in one terminal
window. Leave this open for the remainder of the build process. Open another terminal, go to
`synit/packaging`, and run `make keyfile`. Then, run `make ARCH=x86_64`.

Hopefully the build will complete successfully. Once it has done so, change to `synit/scripts`
and run `./upload-bundle.sh pm-qemu`. Then log in to the emulated device and run the
`./transmogrify.sh` script from the `/home/user` directory. Reboot the device. When it comes
back, you will find that it is running Synit (check `ps` output to see that `synit-pid1` is in
fact PID 1).

## Build and install the user interface packages

To build (and run locally) the SqueakPhone image, ensure your Unix user is in the `input`
group. Follow the instructions in the [SqueakPhone
README](https://git.syndicate-lang.org/tonyg/squeak-phone); namely, first [install
squeaker](https://github.com/tonyg/squeaker), check out the [squeak-phone
repository](https://git.syndicate-lang.org/tonyg/squeak-phone), and run `make images/current`
inside it.

Then, on the device, create a file `/home/user/dpi.override` containing just `256`. On the host
machine, send your image to the device with `./push-image-to-phone.sh pm-qemu`. It should
automatically start.
