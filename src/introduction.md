# Synit is a Reactive Operating System

## Welcome!

Synit is an experiment in applying **pervasive reactivity** and **object capabilities** to the
[System Layer](./glossary.md#system-layer) of an operating system for personal computers,
including laptops, desktops, and mobile phones. Its [architecture](./architecture.md) follows
the principles of the [Syndicated Actor Model](./glossary.md#syndicated-actor-model).

Synit builds upon the Linux kernel, but replaces many pieces of
familiar Linux software, including `systemd`, `NetworkManager`,
`D-Bus`, and so on. It makes use of many concepts that will be
familiar to Linux users, but also incorporates many ideas drawn from
programming languages and operating systems not closely connected with
Linux's Unix heritage.

 - Project homepage: <https://synit.org/>
 - Source code: <https://git.syndicate-lang.org/synit/>

## Quickstart

You can [run Synit on an emulated device](./emulator.md), or if you have a mobile phone or
computer capable of running [PostmarketOS](https://postmarketos.org/), then you can
[install](./install.md) the software on your device to try it out.

See the [installation instructions](./install.md) for a list of
supported devices.

## Acknowledgements

Much initial work on Synit was made possible by a generous grant from
the [NLnet Foundation](https://nlnet.nl/) as part of the [NGI
Zero](https://nlnet.nl/NGI0/) [PET](https://nlnet.nl/PET/) programme.
Please see "[Structuring the System Layer with Dataspaces
(2021)](https://syndicate-lang.org/projects/2021/system-layer/)" for
details of the funded project.

## Copyright and License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img
    alt="Creative Commons License"
    style="border-width:0; margin-right:0.33em; float:left;"
    src="https://i.creativecommons.org/l/by/4.0/88x31.png"></a>
This manual is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).  
Copyright © 2021–2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>.

The Synit programs and source code are separately licensed. Please see the source code for
details.
