# The `preserves-tools` package

The `preserves-tools` package includes useful command-line utilities for working with
[Preserves](../guide/preserves.md) values and schemas.

At present, it includes the [`preserves-tool` Swiss-army-knife
utility][preserves-tool], which is useful for

 - converting between text and binary Preserves syntaxes;
 - pretty-printing (indenting) text Preserves syntax;
 - manipulating Preserves annotations;
 - breaking down and filtering Preserves documents using [preserves
   path](https://preserves.dev/preserves-path.html) selectors;
 - and so on.

See also the [`preserves-tool` documentation][preserves-tool].

[preserves-tool]: https://preserves.dev/doc/preserves-tool.html
