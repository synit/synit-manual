all: book

book:
	mdbook build

clean:
	rm -rf book

deploy:
	(. ./.envrc; $(MAKE) all)
