# Synit Manual

## Welcome!

Synit is an experiment in applying **pervasive reactivity** and **object capabilities** to the
[System Layer](./src/glossary.md#system-layer) of an operating system for personal computers,
including laptops, desktops, and mobile phones.

This is the manual for the system.

 - Project homepage: <https://synit.org/>
 - Source code: <https://git.syndicate-lang.org/synit/>

## Repository contents

The manual text:

 - [`src/`](src/) contains the markdown files making up the main source text of the manual.

Support software:

 - [`book.prs`](book.prs) and the corresponding binary, `book.prb`, describe the protocol used
   for communication between mdbook and its plugins.
 - [`book.toml`](book.toml) is the configuration file for mdbook.
 - [`mdbook_plugin.py`](mdbook_plugin.py) is a support library for writing mdbook plugins.
   - [`mdbook-ditaa`](mdbook-ditaa) is a plugin for mdbook that renders (and caches) ditaa
   diagrams from the source text into SVG for the built website.
   - [`mdbook-pseudocode`](mdbook-pseudocode) is a plugin for mdbook that makes a pretty
   rendering of pseudocode snippets in the source text.

After running a build, the `./book` subdirectory contains the rendered result.

## Building the manual

You will need several tools:

 - [mdbook](https://rust-lang.github.io/mdBook/)

 - a JVM and [ditaa version 0.11.0](https://github.com/stathissideris/ditaa/releases)

 - Python 3.x and the [Preserves python package](https://pypi.org/project/preserves/) (`pip
install -U preserves`)

Running `make` or `make all` should do the trick. See also the [`Makefile`](Makefile) and
[`.envrc`](.envrc).

## Acknowledgements

Much initial work on Synit was made possible by a generous grant from
the [NLnet Foundation](https://nlnet.nl/) as part of the [NGI
Zero](https://nlnet.nl/NGI0/) [PET](https://nlnet.nl/PET/) programme.
Please see "[Structuring the System Layer with Dataspaces
(2021)](https://syndicate-lang.org/projects/2021/system-layer/)" for
details of the funded project.

## Copyright and License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img
    alt="Creative Commons License"
    style="border-width:0; margin-right:0.33em; float:left;"
    src="https://i.creativecommons.org/l/by/4.0/88x31.png"></a>
This manual is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).  
Copyright © 2021–2022 Tony Garnock-Jones <tonyg@leastfixedpoint.com>.

The Synit programs and source code are separately licensed. Please see the source code for
details.
