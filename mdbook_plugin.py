import sys
import subprocess
import re

from preserves import preserve, parse, stringify
from preserves.schema import load_schema_file, extend

try:
    schema = load_schema_file('./book.prb').book
except FileNotFoundError:
    subprocess.check_output(['sh', '-c', 'preserves-schemac --no-bundle .:book.prs > book.prb'])
    schema = load_schema_file('./book.prb').book

@extend(schema.Book)
def expand(self, plugin):
    for i in self.sections:
        i.expand(plugin)

@extend(schema.BookItem.chapter)
def expand(self, plugin):
    self.value.expand(plugin)

@extend(schema.BookItem._ALL)
def expand(self, plugin):
    pass

@extend(schema.Chapter)
def expand(self, plugin):
    if self.source_path.VARIANT.name == 'present':
        plugin.expand_chapter_content(self)
    for i in self.sub_items:
        i.expand(plugin)

def structure(items):
    answer = []
    for i in items:
        if i.VARIANT.name == 'chapter':
            a = structure(i.value.sub_items)
            answer.append({ i.value.name: a })
    return answer

class Plugin:
    def __init__(self):
        self.context = None

    def expand_chapter_content(self, chapter):
        '''Override this method to macroexpand chapter content.'''
        pass

    def expand_codeblock(self, language_re, expander, s, *args):
        return re.sub('^```' + language_re + '\n(.*?)\n```\n', lambda m: expander(m, *args), s, 0, re.M | re.S)

    def main(self, argv):
        if argv[-2:-1] == ['supports']:
            sys.exit(0 if argv[-1] == 'html' else 1)
        output_prefix = argv[1] if len(argv) > 1 else 'figures'

        raw = parse(sys.stdin.read())
        # sys.stderr.write(stringify(raw, indent=2) + '\n')
        i = schema.PreprocessorInput.decode(raw)
        self.context = i.context
        # sys.stderr.write(stringify(preserve(self.context), indent=2) + '\n')
        i.book.expand(self)
        print(stringify(preserve(i.book), with_commas=True))
